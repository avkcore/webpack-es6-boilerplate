class Service {
  constructor(name) {
    this.name = name;
  }

  getName() {
    return this.name;
  }
}

export default Service;
