import Service from './services/service';
import log from './helpers/logger';

const timerPromise = someNum =>
  new Promise((resolve, reject) => {
    if (someNum < 1) {
      reject();
    }
    setTimeout(() => resolve(someNum), 2000);
  });

async function resolveTimer() {
  const timer = await (timerPromise(5));
  log(timer);
}

const service = new Service('ansible');
log(service.getName());

resolveTimer();
